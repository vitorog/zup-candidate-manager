import * as types from '../actions/types'

const initialState = {}

const user = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FETCH_USER:
      return payload
    default:
      return state
  }
}

export default user
