import * as types from '../actions/types'

const initialState = ''

const search = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SEARCH_CANDIDATE:
      return payload
    default:
      return state
  }
}

export default search
