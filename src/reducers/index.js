import { combineReducers } from 'redux'
import candidates from './candidates'
import user from './user'
import search from './search'

export default combineReducers({
  candidates,
  user,
  search,
})
