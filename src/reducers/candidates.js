import * as types from '../actions/types'

const initialState = {
  all: [],
  matched: [],
  removed: [],
}

const candidates = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FETCH_ALL_CANDIDATES:
      return {
        ...state,
        all: payload,
      }
    case types.FETCH_MORE_CANDIDATES:
      return {
        ...state,
        all: [...state.all, ...payload],
      }
    case types.FETCH_ALL_MATCHED_CANDIDATES:
      return {
        ...state,
        matched: payload,
      }
    case types.FETCH_ALL_REMOVED_CANDIDATES:
      return {
        ...state,
        removed: payload,
      }
    case types.SET_MATCHED_CANDIDATE:
      return {
        ...state,
        matched: [...state.matched, payload],
      }
    case types.SET_REMOVED_CANDIDATE:
      return {
        ...state,
        removed: [...state.removed, payload],
      }
    case types.SET_CANDIDATE:
      return {
        ...state,
        all: [...state.all, payload],
      }
    default:
      return state
  }
}

export default candidates
