import React from 'react'
import PropTypes from 'prop-types'
import classNames from './Avatar.css'

const Avatar = props => (
  <div style={{ width: 45, height: 45, borderRadius: 100 }}>
    <img
      className={classNames.avatarContainer}
      style={{
        alignSelf: 'center',
      }}
      src={props.avatar}
      alt="Default"
    />
  </div>
)

Avatar.defaultProps = {
  avatar: '/assets/custom-profile.svg',
}

Avatar.propTypes = {
  avatar: PropTypes.string,
}

export default Avatar
