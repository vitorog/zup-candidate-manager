import React, { Component } from 'react'
import { Card } from 'material-ui/Card'
import { Icon } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from './CandidateList.css'
import Item from './Item'
import Description from './Description'

class CandidateList extends Component {
  constructor(props) {
    super(props)
    this.onItemClick = this.onItemClick.bind(this)
  }

  onItemClick(item) {
    const to = {
      pathname: 'detail',
      state: item,
    }
    this.props.history.push(to)
  }

  render() {
    if (this.props.candidates.length === 0) {
      return (
        <Card className={classNames.container}>
          <div
            style={{
              display: 'flex',
              width: '100%',
              justifyContent: 'center',
              padding: 40,
            }}
          >
            <Description>
              Nenhum candidato encontrado... <Icon name="meh" />
            </Description>
          </div>
        </Card>
      )
    }
    return (
      <Card className={classNames.container}>
        <ul role="group" style={{ listStyleType: 'none' }} className={classNames.listItem}>
          {this.props.candidates.map((item, i) => (
            <li
              key={i}
              onClick={() => this.onItemClick(item)}
              onKeyPress={() => {}}
              role="menuitem"
            >
              <Item
                customKey={i}
                thumb={item.thumb}
                name={item.firstName}
                email={item.email}
                phone={item.phone}
                city={item.location}
                mode={this.props.mode}
                removeCandidate={() => this.props.removeCandidate(item)}
                moveCandidateToAll={() => this.props.moveCandidateToAll(item)}
                matchCandidate={() => this.props.matchCandidate(item)}
              />
            </li>
        ))}
        </ul>
      </Card>
    )
  }
}

CandidateList.defaultPropts = {
  mode: 'ALL',
}

CandidateList.propTypes = {
  candidates: PropTypes.array,
  mode: PropTypes.string,
  history: PropTypes.object,
  removeCandidate: PropTypes.func,
  moveCandidateToAll: PropTypes.func,
  matchCandidate: PropTypes.func,
}

export default withRouter(CandidateList)
