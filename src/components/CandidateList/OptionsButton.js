import React, { Component } from 'react'
import PropTypes from 'prop-types'
import FlatButton from 'material-ui/FlatButton'
import CustomIcon from '../CustomIcon'
import { ICONS, VIEW_BOX } from '../../assets/icons'
import classNames from './CandidateList.css'

class OptionsButton extends Component {
  constructor(props) {
    super(props)
    this.renderButtons = this.renderButtons.bind(this)
    this.onRemove = this.onRemove.bind(this)
    this.onMoveToAll = this.onMoveToAll.bind(this)
    this.onMatch = this.onMatch.bind(this)
  }

  onRemove(event) {
    this.props.removeCandidate()
    event.stopPropagation()
    event.preventDefault()
  }

  onMoveToAll(event) {
    this.props.moveCandidateToAll()
    event.stopPropagation()
    event.preventDefault()
  }

  onMatch(event) {
    this.props.matchCandidate()
    event.stopPropagation()
    event.preventDefault()
  }

  renderButtons() {
    const buttons = []
    if (this.props.mode === 'ALL' || this.props.mode === 'MATCHED') {
      buttons.push(<FlatButton
        key={`TRASH${this.props.customKey}`}
        backgroundColor="transparent"
        hoverColor="transparent"
        onClick={this.onRemove}
        disableTouchRipple
        className={classNames.remove}
        style={{
          minWidth: 16,
          padding: 0,
        }}
        icon={
          <CustomIcon
            color="#ababab"
            hoverColor="#b3bf2e"
            width={16}
            height={16}
            path={ICONS.trash}
            viewBox={VIEW_BOX.trash}
          />
        }
      />)
      buttons.push(<div
        style={{ marginLeft: 10, marginRight: 14 }}
        key={`DIVONE${this.props.customKey}`}
      />)
    }
    if (this.props.mode === 'MATCHED' || this.props.mode === 'TRASH') {
      buttons.push(<FlatButton
        key={`ALL${this.props.customKey}`}
        backgroundColor="transparent"
        hoverColor="transparent"
        disableTouchRipple
        onClick={this.onMoveToAll}
        style={{
          minWidth: 16,
          padding: 0,
        }}
        icon={
          <CustomIcon
            color="#ababab"
            hoverColor="#b3bf2e"
            width={16}
            height={16}
            path={ICONS.all}
            viewBox={VIEW_BOX.all}
          />
        }
      />)
      buttons.push(<div
        style={{ marginRight: 14 }}
        key={`DIVTWO${this.props.customKey}`}
      />)
    }
    if (this.props.mode === 'ALL' || this.props.mode === 'TRASH') {
      buttons.push(<FlatButton
        key={`MATCH${this.props.customKey}`}
        backgroundColor="transparent"
        hoverColor="transparent"
        onClick={this.onMatch}
        disableTouchRipple
        style={{
          minWidth: 16,
        }}
        icon={
          <CustomIcon
            color="#ababab"
            hoverColor="#b3bf2e"
            width={16}
            height={16}
            path={ICONS.checked}
            viewBox={VIEW_BOX.checked}
          />
        }
      />)
      buttons.push(<div
        style={{ marginRight: 14 }}
        key={`DIVTHREE${this.props.customKey}`}
      />)
    }
    return buttons
  }

  render() {
    return (
      <div style={{ display: 'flex', width: '10vw' }}>
        {this.renderButtons()}
      </div>
    )
  }
}

OptionsButton.defaultProps = {
  removeCandidate: () => {},
  moveCandidateToAll: () => {},
  matchCandidate: () => {},
}

OptionsButton.propTypes = {
  mode: PropTypes.string,
  customKey: PropTypes.number,
  removeCandidate: PropTypes.func,
  moveCandidateToAll: PropTypes.func,
  matchCandidate: PropTypes.func,
}

export default OptionsButton
