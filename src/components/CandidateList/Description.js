import React from 'react'
import PropTypes from 'prop-types'
import classNames from './Text.css'

const Description = props => (
  <div>
    <span className={classNames.description}>
      {props.children}
    </span>
  </div>
)

Description.propTypes = {
  children: PropTypes.node,
}

export default Description
