import React from 'react'
import PropTypes from 'prop-types'
import Description from './Description'
import Title from './Title'
import Avatar from '../Avatar'
import classNames from './CandidateList.css'
import OptionsButton from './OptionsButton'

const Item = props => (
  <div className={classNames.item}>
    <div className={classNames.profile}>
      <Avatar avatar={props.thumb} />
      <div style={{ marginRight: 24 }} />
      <Title>
        {props.name}
      </Title>
    </div>
    <div className={classNames.description}>
      <div style={{ width: '19vw' }}>
        <Description>
          {props.email}
        </Description>
      </div>
      <div style={{ marginRight: 20 }} />
      <Description>
        {props.phone}
      </Description>
      <div style={{ marginRight: 20 }} />
      <Description>
        {props.city}
      </Description>
    </div>
    <div className={classNames.buttons}>
      <OptionsButton
        customKey={props.customKey}
        mode={props.mode}
        removeCandidate={props.removeCandidate}
        moveCandidateToAll={props.moveCandidateToAll}
        matchCandidate={props.matchCandidate}
      />
    </div>
  </div>
)

Item.propTypes = {
  thumb: PropTypes.string,
  name: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
  city: PropTypes.string,
  mode: PropTypes.string,
  customKey: PropTypes.number,
  removeCandidate: PropTypes.func,
  moveCandidateToAll: PropTypes.func,
  matchCandidate: PropTypes.func,
}

export default Item
