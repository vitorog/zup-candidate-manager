import React from 'react'
import PropTypes from 'prop-types'
import classNames from './Text.css'

const Title = props => (
  <div>
    <span className={classNames.title}>
      {props.children}
    </span>
  </div>
)

Title.propTypes = {
  children: PropTypes.node,
}

export default Title
