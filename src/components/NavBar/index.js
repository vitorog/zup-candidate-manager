import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import SearchBar from './SearchBar'
import Avatar from '../Avatar'
import classNames from './NavBar.css'

const NavBar = props => (
  <div className={classNames.navBar}>
    <Link to="/">
      <img src="/assets/Logo_Zup.png" alt="Zup logo" />
    </Link>
    <SearchBar handleTextChange={props.handleTextChange} textValue={props.textValue} />
    <Avatar avatar={props.userThumb} />
  </div>
)

NavBar.propTypes = {
  userThumb: PropTypes.string,
  handleTextChange: PropTypes.func,
  textValue: PropTypes.string,
}

export default NavBar
