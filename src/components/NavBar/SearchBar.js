import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from './NavBar.css'

class SearchBar extends Component {
  constructor(props) {
    super(props)
    this.onChangeText = this.onChangeText.bind(this)
  }

  onChangeText(event) {
    const { target } = event
    const { value } = target
    return this.props.handleTextChange(value)
  }


  render() {
    return (
      <div>
        <input
          className={classNames.itemInput}
          type="search"
          value={this.props.textValue}
          onChange={this.onChangeText}
          placeholder="Buscar"
        />
      </div>
    )
  }
}


SearchBar.defaultProps = {
  textValue: '',
}

SearchBar.propTypes = {
  textValue: PropTypes.string,
  handleTextChange: PropTypes.func,
}

export default SearchBar
