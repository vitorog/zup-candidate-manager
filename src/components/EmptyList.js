import React from 'react'
import { Icon } from 'semantic-ui-react'
import Description from './CandidateList/Description'

const EmptyList = () => (
  <div style={{ display: 'flex', marginTop: 40 }}>
    <Description>
      Nenhum candidato no momento... <Icon name="meh" />
    </Description>
  </div>
)

export default EmptyList
