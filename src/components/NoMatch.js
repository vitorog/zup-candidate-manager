import React from 'react'
import { Link } from 'react-router-dom'
import { Icon } from 'semantic-ui-react'
import Title from './CandidateList/Title'
import Description from './CandidateList/Description'

const NoMatch = () => (
  <div style={{
    display: 'flex',
    width: '100vw',
    height: '50vh',
    justifyContent: 'center',
    alignItems: 'center',
    }}
  >
    <Link to="/">
      <Title>
        Page not found <Icon name="meh" />
      </Title>
      <Description>
        Click here to get back! <Icon name="heart" color="red" />
      </Description>
    </Link>
  </div>
)

export default NoMatch
