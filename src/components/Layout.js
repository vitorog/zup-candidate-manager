import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import NavBar from './NavBar'
import SideMenu from './SideMenu'

const _Layout = props => (
  <div>
    <NavBar
      userThumb={props.user.thumb}
      handleTextChange={props.handleTextChange}
      textValue={props.textValue}
    />
    <div style={{ display: 'flex' }}>
      <SideMenu />
      <div style={{ justifyContent: 'center', display: 'flex', width: '85vw' }}>
        {props.children}
      </div>
    </div>
  </div>
)

_Layout.propTypes = {
  children: PropTypes.node,
  user: PropTypes.object,
  handleTextChange: PropTypes.func,
  textValue: PropTypes.string,
}


const mapStateToProps = state => ({
  user: state.user || {},
})

const Layout = connect(mapStateToProps, null)(_Layout)
export default Layout
