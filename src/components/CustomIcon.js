import React from 'react'
import PropTypes from 'prop-types'
import SvgIcon from 'material-ui/SvgIcon'

const CustomIcon = props => (
  <SvgIcon
    style={{
      width: props.width,
      height: props.height,
      ...props.style,
    }}
    color={props.color}
    hoverColor={props.hoverColor}
    onMouseEnter={props.onMouseEnter}
    viewBox={props.viewBox}
  >
    <path
      d={props.path}
    />
    {
      props.secondPath &&
      <path
        d={props.secondPath}
      />
    }
  </SvgIcon>
)

CustomIcon.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  path: PropTypes.string,
  color: PropTypes.string,
  hoverColor: PropTypes.string,
  style: PropTypes.object,
  viewBox: PropTypes.string,
  onMouseEnter: PropTypes.func,
  secondPath: PropTypes.string,
}

CustomIcon.defaultProps = {
  onMouseEnter: () => {},
}

export default CustomIcon
