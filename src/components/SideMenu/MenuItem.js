import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import classNames from './SideMenu.css'
import CustomIcon from '../CustomIcon'

class MenuItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isActive: false,
    }
    this.isActive = this.isActive.bind(this)
  }

  isActive(match) {
    if (!match || match.path !== this.props.url) {
      if (this.state.isActive) {
        this.setState({ isActive: false })
      }
      return false
    }
    if (match.path === this.props.url && !this.state.isActive) {
      this.setState({
        isActive: true,
      })
    }
    return true
  }
  render() {
    return (
      <div className={classNames.itemContainer}>
        <NavLink
          strict={this.props.strict}
          exact={this.props.exact}
          to={this.props.url}
          activeClassName={classNames.selectedItemText}
          className={classNames.itemText}
          isActive={this.isActive}
        >
          <CustomIcon
            style={{
              alignSelf: 'center',
              width: 20,
              height: 20,
              marginRight: 12,
            }}
            width={20}
            height={20}
            color={this.state.isActive ? '#b3bf2e' : '#88878a'}
            path={this.props.icon}
            alt="Menu item icon"
            viewBox={this.props.viewBox}
          />
          {this.props.name}
        </NavLink>
      </div>
    )
  }
}

MenuItem.propTypes = {
  strict: PropTypes.bool,
  exact: PropTypes.bool,
  name: PropTypes.string,
  url: PropTypes.string,
  icon: PropTypes.string,
  viewBox: PropTypes.string,
}

export default MenuItem
