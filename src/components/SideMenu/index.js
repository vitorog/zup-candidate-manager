import React, { Component } from 'react'
import MenuItem from './MenuItem'
import classNames from './SideMenu.css'
import { ICONS, VIEW_BOX } from '../../assets/icons'


export default class SideMenu extends Component {
  constructor() {
    super()
    this.state = {
      menuItems: [
        {
          name: 'Todos',
          url: '/',
          type: 'exact',
          icon: ICONS.all,
          viewBox: VIEW_BOX.all,
        }, {
          name: 'Atendidos',
          url: '/matched',
          type: 'exact',
          icon: ICONS.matched,
          viewBox: VIEW_BOX.matched,
        }, {
          name: 'Lixeira',
          url: '/declined',
          type: 'exact',
          icon: ICONS.trash,
          viewBox: VIEW_BOX.trash,
        },
      ],
    }
  }

  render() {
    return (
      <div className={classNames.container}>
        <ul style={{ listStyleType: 'none', padding: 0, paddingTop: 4 }}>
          {this.state.menuItems.map((item, i) => (
            <li key={i}>
              <MenuItem
                exact={item.type === 'exact'}
                strict={item.type === 'strict'}
                name={item.name}
                url={item.url}
                icon={item.icon}
                viewBox={item.viewBox}
              />
            </li>
        ))}
        </ul>
      </div>
    )
  }
}
