import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import _remove from 'lodash.remove'
import deepequal from 'deep-equal'
import _filter from 'lodash.filter'
import CandidateList from '../components/CandidateList'
import EmptyList from '../components/EmptyList'
import {
  newCandidate,
  setAllRemovedCandidates,
  newMatchedCandidate,
} from '../actions/candidates'
import Layout from '../components/Layout'
import searchCandidate from '../actions/search'

class _Declined extends Component {
  constructor(props) {
    super(props)
    this.state = {
      candidates: _filter(
        this.props.candidates,
        data => (data.fullName.includes(this.props.search)
        || data.email.includes(this.props.search)),
      ),
    }
    this.onMoveCandidateToAll = this.onMoveCandidateToAll.bind(this)
    this.onMatch = this.onMatch.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.candidates
      && !deepequal(nextProps.candidates, this.props.candidates)) {
      let { candidates } = nextProps
      if (nextProps.search !== '') {
        candidates = _filter(
          nextProps.candidates,
          data => (data.fullName.includes(nextProps.search)
          || data.email.includes(nextProps.search)),
        )
      }
      this.setState({
        candidates,
      })
    }
    if (nextProps.search !== this.props.search) {
      const candidates = _filter(
        nextProps.candidates,
        data => (data.fullName.includes(nextProps.search)
        || data.email.includes(nextProps.search)),
      )
      this.setState({
        candidates,
      })
    }
  }

  onMoveCandidateToAll(item) {
    const myNewArray = _remove(this.props.candidates, candidate => (
      candidate.email !== item.email
    ))
    this.props.newCandidate(item)
    this.props.setAllRemovedCandidates(myNewArray)
  }

  onMatch(item) {
    const myNewArray = _remove(this.props.candidates, candidate => (
      candidate.email !== item.email
    ))
    this.props.newMatchedCandidate(item)
    this.props.setAllRemovedCandidates(myNewArray)
  }

  render() {
    if (this.props.candidates.length === 0) {
      return (
        <Layout>
          <EmptyList />
        </Layout>
      )
    }
    return (
      <Layout
        textValue={this.props.search}
        handleTextChange={this.props.searchCandidate}
      >
        <CandidateList
          candidates={this.state.candidates}
          mode="TRASH"
          moveCandidateToAll={this.onMoveCandidateToAll}
          matchCandidate={this.onMatch}
        />
      </Layout>
    )
  }
}

_Declined.propTypes = {
  candidates: PropTypes.array,
  newCandidate: PropTypes.func,
  newMatchedCandidate: PropTypes.func,
  setAllRemovedCandidates: PropTypes.func,
  search: PropTypes.string,
  searchCandidate: PropTypes.func,
}

_Declined.defaultProps = {
  search: '',
}

const mapStateToProps = state => ({
  candidates: state.candidates.removed,
  search: state.search,
})

const mapActionsToProps = dispatch => ({
  newCandidate(candidate) {
    dispatch(newCandidate(candidate))
  },
  newMatchedCandidate(candidate) {
    dispatch(newMatchedCandidate(candidate))
  },
  setAllRemovedCandidates(candidates) {
    dispatch(setAllRemovedCandidates(candidates))
  },
  searchCandidate(value) {
    dispatch(searchCandidate(value))
  },
})

const Declined = connect(mapStateToProps, mapActionsToProps)(_Declined)
export default Declined
