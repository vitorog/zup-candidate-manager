import React from 'react'
import PropTypes from 'prop-types'
import { container, label, value } from './CandidateInfo.css'

const CandidateInfo = (props) => {
  const textTransform = props.capitalize ? { textTransform: 'capitalize' } : {}
  return (
    <div className={container}>
      <span className={label}>
        {props.label}
      </span>
      <span className={value} style={textTransform}>
        {props.value}
      </span>
    </div>
  )
}

CandidateInfo.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  capitalize: PropTypes.bool,
}

export default CandidateInfo
