import React from 'react'
import PropTypes from 'prop-types'
import FloatingActionButton from 'material-ui/FloatingActionButton'

const FabButton = props => (
  <div>

    <FloatingActionButton
      iconStyle={{ fill: '#2b3d4f', width: 22, height: 22 }}
      disabled={props.disabled}
      style={props.buttonStyle}
      backgroundColor={props.buttonStyle.backgroundColor}
      onClick={props.onClick}
      zDepth={props.zDepth}
    >
      {props.children}
    </FloatingActionButton>
  </div>
)

FabButton.defaultProps = {
  buttonStyle: {
    backgroundColor: '#ffffff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 32,
    width: 32,
  },
  disabled: false,
  zDepth: 2,
}

FabButton.propTypes = {
  children: PropTypes.node,
  buttonStyle: PropTypes.object,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  zDepth: PropTypes.number,
}

export default FabButton
