import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card } from 'material-ui/Card'
import BackIcon from 'material-ui/svg-icons/image/navigate-before'
import FabButton from './components/FabButton'
import classNames from './Detail.css'
import CandidateInfo from './components/CandidateInfo'
import CustomIcon from '../../components/CustomIcon'
import { ICONS, VIEW_BOX } from '../../assets/icons'


class Detail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      NAME: 'Hi, My name is',
      EMAIL: 'My email address is',
      BIRTHDAY: 'My birthday is',
      ADDRESS: 'My address is',
      PHONE: 'My phone number is',
      PASS: 'My password is',
      currentState: 'NAME',
      value: this.props.location && this.props.location.state
              && this.props.location.state.fullName,
    }
    this.onHover = this.onHover.bind(this)
    this.goBack = this.goBack.bind(this)
  }

  onHover(type, value) {
    if (this.state.currentState !== type) {
      this.setState({ currentState: type, value })
    }
  }

  goBack() {
    this.props.history.goBack()
  }

  render() {
    return (
      <div className={classNames.container}>
        <div
          style={{
            position: 'absolute',
            left: 22,
            top: 20,
          }}
        >
          <FabButton
            onClick={this.goBack}
          >
            <BackIcon />
          </FabButton>
        </div>
        <Card
          style={{
            width: '50vw',
            height: '70vh',
            backgroundColor: '#fff',
            position: 'relative',
          }}
        >
          <div className={classNames.header} />
          <div className={classNames.body}>
            <img
              className={classNames.avatarContainer}
              style={{
                alignSelf: 'center',
              }}
              src={
                this.props.location &&
                this.props.location.state &&
                this.props.location.state.picture
              }
              alt="Candidate profile"
            />
            <div style={{ marginTop: 38 }} />
            <CandidateInfo
              capitalize={this.state.currentState === 'NAME' || this.state.currentState === 'ADDRESS'}
              label={this.state[this.state.currentState]}
              value={this.state.value}
            />
            <div
              style={{
                display: 'flex',
                width: '60%',
                marginTop: 54,
                justifyContent: 'space-around',
              }}
            >
              <CustomIcon
                color={this.state.currentState === 'NAME' ? '#b3bf2e' : '#ababab'}
                hoverColor="#b3bf2e"
                width={34}
                height={34}
                path={ICONS.user}
                viewBox={VIEW_BOX.user}
                onMouseEnter={() => this.onHover('NAME', this.props.location.state.fullName)}
              />
              <CustomIcon
                color={this.state.currentState === 'EMAIL' ? '#b3bf2e' : '#ababab'}
                hoverColor="#b3bf2e"
                width={34}
                height={34}
                path={ICONS.mail_1}
                secondPath={ICONS.mail_2}
                viewBox={VIEW_BOX.mail}
                onMouseEnter={() => this.onHover('EMAIL', this.props.location.state.email)}
              />
              <CustomIcon
                color={this.state.currentState === 'BIRTHDAY' ? '#b3bf2e' : '#ababab'}
                hoverColor="#b3bf2e"
                width={34}
                height={34}
                path={ICONS.calendar_1}
                secondPath={ICONS.calendar_2}
                viewBox={VIEW_BOX.calendar}
                onMouseEnter={() => this.onHover('BIRTHDAY', this.props.location.state.birthday)}
              />
              <CustomIcon
                color={this.state.currentState === 'ADDRESS' ? '#b3bf2e' : '#ababab'}
                hoverColor="#b3bf2e"
                width={34}
                height={34}
                path={ICONS.location_1}
                secondPath={ICONS.location_2}
                viewBox={VIEW_BOX.location}
                onMouseEnter={() => this.onHover('ADDRESS', this.props.location.state.address)}
              />
              <CustomIcon
                color={this.state.currentState === 'PHONE' ? '#b3bf2e' : '#ababab'}
                hoverColor="#b3bf2e"
                width={34}
                height={34}
                path={ICONS.phone}
                viewBox={VIEW_BOX.phone}
                onMouseEnter={() => this.onHover('PHONE', this.props.location.state.phone)}
              />
              <CustomIcon
                color={this.state.currentState === 'PASS' ? '#b3bf2e' : '#ababab'}
                hoverColor="#b3bf2e"
                width={34}
                height={34}
                path={ICONS.key_1}
                secondPath={ICONS.key_2}
                viewBox={VIEW_BOX.key}
                onMouseEnter={() => this.onHover('PASS', this.props.location.state.password)}
              />
            </div>
          </div>
        </Card>
      </div>
    )
  }
}

Detail.propTypes = {
  location: PropTypes.object,
  history: PropTypes.object,
}

export default Detail
