import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import _remove from 'lodash.remove'
import deepequal from 'deep-equal'
import _filter from 'lodash.filter'
import CandidateList from '../components/CandidateList'
import EmptyList from '../components/EmptyList'
import {
  newDeletedCandidate,
  setAllMatchedCandidates,
  newCandidate,
} from '../actions/candidates'
import Layout from '../components/Layout'
import searchCandidate from '../actions/search'

class _Matched extends Component {
  constructor(props) {
    super(props)
    this.state = {
      candidates: _filter(
        this.props.candidates,
        data => (data.fullName.includes(this.props.search)
        || data.email.includes(this.props.search)),
      ),
    }
    this.onRemove = this.onRemove.bind(this)
    this.onMoveCandidateToAll = this.onMoveCandidateToAll.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.candidates
      && !deepequal(nextProps.candidates, this.props.candidates)) {
      let { candidates } = nextProps
      if (nextProps.search !== '') {
        candidates = _filter(
          nextProps.candidates,
          data => (data.fullName.includes(nextProps.search)
          || data.email.includes(nextProps.search)),
        )
      }
      this.setState({
        candidates,
      })
    }
    if (nextProps.search !== this.props.search) {
      const candidates = _filter(
        nextProps.candidates,
        data => (data.fullName.includes(nextProps.search)
        || data.email.includes(nextProps.search)),
      )
      this.setState({
        candidates,
      })
    }
  }

  onRemove(item) {
    const myNewArray = _remove(this.props.candidates, candidate => (
      candidate.email !== item.email
    ))
    this.props.newDeletedCandidate(item)
    this.props.setAllMatchedCandidates(myNewArray)
  }

  onMoveCandidateToAll(item) {
    const myNewArray = _remove(this.props.candidates, candidate => (
      candidate.email !== item.email
    ))
    this.props.newCandidate(item)
    this.props.setAllMatchedCandidates(myNewArray)
  }

  render() {
    if (this.props.candidates.length === 0) {
      return (
        <Layout>
          <EmptyList />
        </Layout>
      )
    }
    return (
      <Layout
        textValue={this.props.search}
        handleTextChange={this.props.searchCandidate}
      >
        <CandidateList
          candidates={this.state.candidates}
          mode="MATCHED"
          removeCandidate={this.onRemove}
          moveCandidateToAll={this.onMoveCandidateToAll}
        />
      </Layout>
    )
  }
}

_Matched.propTypes = {
  candidates: PropTypes.array,
  newDeletedCandidate: PropTypes.func,
  newCandidate: PropTypes.func,
  setAllMatchedCandidates: PropTypes.func,
  search: PropTypes.string,
  searchCandidate: PropTypes.func,
}

_Matched.defaultProps = {
  search: '',
}

const mapStateToProps = state => ({
  candidates: state.candidates.matched,
  search: state.search,
})

const mapActionsToProps = dispatch => ({
  newDeletedCandidate(candidate) {
    dispatch(newDeletedCandidate(candidate))
  },
  newCandidate(candidate) {
    dispatch(newCandidate(candidate))
  },
  setAllMatchedCandidates(candidates) {
    dispatch(setAllMatchedCandidates(candidates))
  },
  searchCandidate(value) {
    dispatch(searchCandidate(value))
  },
})

const Matched = connect(mapStateToProps, mapActionsToProps)(_Matched)
export default Matched
