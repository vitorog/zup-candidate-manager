import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import deepequal from 'deep-equal'
import InfiniteScroll from 'react-infinite-scroller'
import _filter from 'lodash.filter'
import _remove from 'lodash.remove'
import CandidateList from '../components/CandidateList'
import EmptyList from '../components/EmptyList'
import {
  newDeletedCandidate,
  setAllCandidates,
  newMatchedCandidate,
  fetchMoreCandidates,
} from '../actions/candidates'
import searchCandidate from '../actions/search'
import Layout from '../components/Layout'

class _Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      candidates: _filter(
        this.props.candidates,
        data => (data.fullName.includes(this.props.search)
        || data.email.includes(this.props.search)),
      ),
    }
    this.onRemove = this.onRemove.bind(this)
    this.onMatch = this.onMatch.bind(this)
    this.fetchMoreCandidates = this.fetchMoreCandidates.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.candidates
      && !deepequal(nextProps.candidates, this.props.candidates)) {
      let { candidates } = nextProps
      if (nextProps.search !== '') {
        candidates = _filter(
          nextProps.candidates,
          data => (data.fullName.includes(nextProps.search)
          || data.email.includes(nextProps.search)),
        )
      }
      this.setState({
        candidates,
      })
    }
    if (nextProps.search !== this.props.search) {
      const candidates = _filter(
        nextProps.candidates,
        data => (data.fullName.includes(nextProps.search)
        || data.email.includes(nextProps.search)),
      )
      this.setState({
        candidates,
      })
    }
  }

  onRemove(item) {
    const myNewArray = _remove(this.props.candidates, candidate => (
      candidate.email !== item.email
    ))
    this.props.newDeletedCandidate(item)
    this.props.setAllCandidates(myNewArray)
  }

  onMatch(item) {
    const myNewArray = _remove(this.props.candidates, candidate => (
      candidate.email !== item.email
    ))
    this.props.newMatchedCandidate(item)
    this.props.setAllCandidates(myNewArray)
  }

  fetchMoreCandidates() {
    if (this.props.search === '') {
      this.props.fetchMoreCandidates(10)
    }
  }

  render() {
    if (this.props.candidates.length === 0) {
      return (
        <Layout>
          <EmptyList />
        </Layout>
      )
    }
    return (
      <InfiniteScroll
        pageStart={0}
        loadMore={this.fetchMoreCandidates}
        hasMore
      >
        <Layout
          textValue={this.props.search}
          handleTextChange={this.props.searchCandidate}
        >
          <CandidateList
            candidates={this.state.candidates}
            mode="ALL"
            removeCandidate={this.onRemove}
            matchCandidate={this.onMatch}
          />
        </Layout>
      </InfiniteScroll>
    )
  }
}

_Home.propTypes = {
  candidates: PropTypes.array,
  newDeletedCandidate: PropTypes.func,
  newMatchedCandidate: PropTypes.func,
  setAllCandidates: PropTypes.func,
  search: PropTypes.string,
  searchCandidate: PropTypes.func,
  fetchMoreCandidates: PropTypes.func,
}

_Home.defaultProps = {
  search: '',
}

const mapStateToProps = state => ({
  candidates: state.candidates.all,
  search: state.search,
})

const mapActionsToProps = dispatch => ({
  newDeletedCandidate(candidate) {
    dispatch(newDeletedCandidate(candidate))
  },
  newMatchedCandidate(candidate) {
    dispatch(newMatchedCandidate(candidate))
  },
  setAllCandidates(candidates) {
    dispatch(setAllCandidates(candidates))
  },
  searchCandidate(value) {
    dispatch(searchCandidate(value))
  },
  fetchMoreCandidates(numberOfCandidates) {
    dispatch(fetchMoreCandidates(numberOfCandidates))
  },
})

const Home = connect(mapStateToProps, mapActionsToProps)(_Home)
export default Home
