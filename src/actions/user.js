import { createAction } from 'redux-actions'
import * as types from './types'
import UserServices from '../services/UserServices'


const fetchUser = () => (
  (dispatch) => {
    UserServices
      .fetchUser()
      .then((data) => {
        const { results } = data
        if (results && results.length > 0) {
          const payload = {
            thumb: results[0].picture && results[0].picture.thumbnail,
          }
          dispatch(createAction(types.FETCH_USER)(payload))
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }
)

export default fetchUser
