import { createAction } from 'redux-actions'
import * as types from './types'

const search = candidate => (
  (dispatch) => {
    dispatch(createAction(types.SEARCH_CANDIDATE)(candidate.toLowerCase()))
  }
)

export default search
