import { createAction } from 'redux-actions'
import * as types from './types'
import CandidatesServices from '../services/CandidatesServices'
import BRASILIAN_STATES from '../utils/brazilianStates'

const candidateMapper = candidates => (
  candidates.map(candidate => ({
    firstName: candidate.name.first,
    fullName: `${candidate.name.first} ${candidate.name.last}`,
    email: candidate.email,
    phone: candidate.phone,
    password: candidate.login.password,
    address: candidate.location.street,
    location: `${candidate.location.city} - ${BRASILIAN_STATES[candidate.location.state]}`,
    thumb: candidate.picture.thumbnail,
    picture: candidate.picture.large,
    birthday: candidate.dob.split(/-|\s|:/).slice(0, 3).reverse().join('/'),
  }))
)

export const fetchNCandidates = numberOfCandidates => (
  (dispatch) => {
    CandidatesServices
      .fetchNCandidates(numberOfCandidates)
      .then((data) => {
        const { results } = data
        if (results && results.length > 0) {
          const candidates = candidateMapper(results)
          dispatch(createAction(types.FETCH_ALL_CANDIDATES)(candidates))
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }
)

export const fetchMoreCandidates = numberOfCandidates => (
  (dispatch) => {
    CandidatesServices
      .fetchNCandidates(numberOfCandidates)
      .then((data) => {
        const { results } = data
        if (results && results.length > 0) {
          const candidates = candidateMapper(results)
          dispatch(createAction(types.FETCH_MORE_CANDIDATES)(candidates))
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }
)

export const setAllCandidates = candidates => (
  (dispatch) => {
    dispatch(createAction(types.FETCH_ALL_CANDIDATES)(candidates))
  }
)

export const setAllMatchedCandidates = candidates => (
  (dispatch) => {
    dispatch(createAction(types.FETCH_ALL_MATCHED_CANDIDATES)(candidates))
  }
)

export const setAllRemovedCandidates = candidates => (
  (dispatch) => {
    dispatch(createAction(types.FETCH_ALL_REMOVED_CANDIDATES)(candidates))
  }
)

export const newCandidate = candidate => (
  (dispatch) => {
    dispatch(createAction(types.SET_CANDIDATE)(candidate))
  }
)

export const newDeletedCandidate = candidate => (
  (dispatch) => {
    dispatch(createAction(types.SET_REMOVED_CANDIDATE)(candidate))
  }
)

export const newMatchedCandidate = candidate => (
  (dispatch) => {
    dispatch(createAction(types.SET_MATCHED_CANDIDATE)(candidate))
  }
)
