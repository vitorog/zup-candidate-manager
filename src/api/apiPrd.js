import { create } from 'apisauce'

const baseURL = 'https://randomuser.me/api/'

const apiPrd = create({
  baseURL,
  headers: {
    'Content-Type': 'application/json',
  },
})

export default apiPrd
