import apiPrd from '../api/apiPrd'

const CandidatesServices = {
  async fetchNCandidates(results) {
    const res = await apiPrd.get(`?results=${results}&nat=br`)
    return res.data
  },
}

export default CandidatesServices
