import apiPrd from '../api/apiPrd'

const UserService = {
  async fetchUser() {
    const res = await apiPrd.get('')
    return res.data
  },
}

export default UserService
