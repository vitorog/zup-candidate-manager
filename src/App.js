import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Home from './screens/Home'
import Matched from './screens/Matched'
import Declined from './screens/Declined'
import NoMatch from './components/NoMatch'
import Detail from './screens/Detail'
import fetchUser from './actions/user'
import { fetchNCandidates } from './actions/candidates'

class _App extends Component {
  componentWillMount() {
    this.props.fetchUser()
    this.props.fetchNCandidates(10)
  }

  render() {
    return (
      <Router>
        <MuiThemeProvider>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/matched" component={Matched} />
            <Route exact path="/declined" component={Declined} />
            <Route exact path="/detail" component={Detail} />
            <Route component={NoMatch} />
          </Switch>
        </MuiThemeProvider>
      </Router>
    )
  }
}

_App.propTypes = {
  fetchUser: PropTypes.func,
  fetchNCandidates: PropTypes.func,
}

const mapActionsToProps = dispatch => ({
  fetchUser() {
    dispatch(fetchUser())
  },
  fetchNCandidates(numberOfCandidates) {
    dispatch(fetchNCandidates(numberOfCandidates))
  },
})

const App = connect(null, mapActionsToProps)(_App)
export default App
